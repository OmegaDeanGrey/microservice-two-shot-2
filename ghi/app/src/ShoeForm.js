import { useState, useEffect } from "react";


function ShoeForm() {
    const noData = {
        manufacturer: "",
        model_name: "",
        color: "",
        picture_url: "",
        bin: "",
    }

    const [bins, setBins] = useState ([])



    const [shoeData, setShoeData] = useState(noData)
    const handleChange = (event) => {
        setShoeData({...shoeData, [event.target.name]: event.target.value});
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        const shoeUrl = "http://localhost:8080/api/shoes/";
        console.log(shoeData)
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify({...shoeData}),
            headers: {"Content-Type": "application.json"}
        }
        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            setShoeData(noData);
            alert(`Welcome, ${newShoe.name}!`);
        } else {
            alert("Something went wrong");
        }
    }

        const getData = async () => {
            const response = await fetch('http://localhost:8100/api/bins/')
            const data = await response.json()
            console.log(data)
            setBins(data.bins)
        }
    
    
        useEffect(() => {
            getData()
        }, [])



    return(
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="text-info">Add New Shoe</h1>
            <form onSubmit={handleSubmit} id="customer-form">
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={shoeData.manufacturer} placeholder="Enter Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                <label htmlFor="manufacturer">Manufacturer</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={shoeData.model_name} placeholder="Enter Shoe Model" required type="text" name="model_name" id="model_name" className="form-control" />
                <label htmlFor="model_name">Model</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={shoeData.color} placeholder="Enter Shoe Color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleChange} value={shoeData.picture_url} placeholder="Enter Shoe Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                <label htmlFor="picture_url">Picture URL</label>
              </div>
              <div className="mb-3">
                    <select onChange={handleChange} required name="bin" id="bin" className="form-select" value={shoeData.bin}>
                    <option value="">Choose a Bin</option>
                        {bins.map(bin => {
                            return (
                                <option key={bin.href} value={bin.href}>
                                {bin.closet_name}
                                </option>
                                 )
                            })}
                    </select>
                </div>
              {/* <div className="form-floating mb-3">
                <input onChange={handleChange} value={shoeData.bin} placeholder="Enter Shoe Bin" required type="text" name="bin" id="bin" className="form-control" />
                <label htmlFor="bin">Bin</label>
              </div> */}
              <button className="btn btn-info">Create</button>
            </form>
          </div>
        </div>
        </div>
    )
}
export default ShoeForm;