import React, { useState, useEffect } from "react";



function ShoeList() {
    const [shoes, setShoes] = useState([])

    


    const getShoeData = async() => {
        const response = await fetch("http://localhost:8080/api/shoes/")
        if (response.ok) {
            const data = await response.json();
            setShoes(data.shoes);
        // } else {
        //     alert("Bilbo")
        // }
        }
    }

    useEffect(()=>{
      getShoeData()
    },[]
    )

    return (

        <div>
            <h1 className="text-info">
                Shoes
            </h1>
            <table className="table table-info">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Color</th>
                        <th>Picture</th>
                        <th>Bin</th>
                    </tr>
                </thead>
                <tbody>
                    {shoes.map(shoe => {
                    return(
                    <tr key={shoe.id}>
                        <td className="text-black"> { shoe.manufacturer }</td>
                        <td> {shoe.model_name} </td>
                        <td> {shoe.color} </td>
                        <td> <img src= {shoe.picture_url} width="100px"></img> </td>
                        <td> {shoe.bin} </td>
                    </tr>
                    ) })}
                </tbody>
            </table>
        </div>
                    
    )}





                    
export default ShoeList;
                    
                