import React, { useState, useEffect } from 'react'
// import "./HatList.css"

export default function HatList() {
    const [hatData, setHatData] = useState([])
    // const []

    const fetchData = async () => {
        const response = await fetch("http://localhost:8090/api/hats")
        const hats = await response.json()
        setHatData(hats.hats)
        console.log(hats.hats)
    }

    const handleDelete = async (hat_id) => {
        const url = `http://localhost:8090/api/hats/${hat_id}`
        const fetchConfig = {
            method: "delete",
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log("hat deleted")
            fetchData()
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    return (
        <>
            <h2>HatList</h2>
            <table className='table table-striped'>
                <tbody>
                    {hatData.map(hat => (
                        <tr key={hat.id} >
                            <td><h2>{hat.style_name}</h2></td>
                            <td>{hat.fabric}</td>
                            <td>{hat.color}</td>
                            <td><img className='img-fluid' src={hat.image} /></td>
                            <td>{hat.location.closet_name}</td>
                            <td><button onClick={() => handleDelete(hat.id)}>Delete</button></td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}
