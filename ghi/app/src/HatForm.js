import React, { useState, useEffect } from 'react'

export default function HatForm() {
    const [locations, setLocations] = useState([])
    const [styleName, setStyleName] = useState("")
    const [fabric, setFabric] = useState("")
    const [color, setColor] = useState("")
    const [image, setImage] = useState("")
    const [location, setLocation] = useState("")



    // fetchData = async () => {
    //     const response = await fetch("")
    // }
    function handleSubmit(e) {
        e.preventDefault()
        console.log("submitted")
    }
    function handleStyleNameChange(e) {
        setStyleName(e.target.value)
    }
    function handleFabricChange(e) {
        setFabric(e.target.value)
    }
    function handleColorChange(e) {
        setColor(e.target.value)
    }
    function handleImageChange(e) {
        setImage(e.target.value)
    }
    function handleLocationChange(e) {
        setLocation(e.target.value)
    }

    async function handleSubmit(e) {
        e.preventDefault()
        const data = {}
        data.style_name = styleName
        data.fabric = fabric
        data.color = color
        data.image = image
        data.location = location
        const haturl = "http://localhost:8090/api/hats/"
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application.json',
            },
        }
        const response = await fetch(haturl, fetchConfig)
        if (response.ok) {
            console.log("hooray")
        }
    }


    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/locations/')
        const data = await response.json()
        setLocations(data.locations)
    }


    useEffect(() => {
        getData()
    }, [])




    return (
        <div>
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>New Hat</h1>
                        <form onSubmit={handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleStyleNameChange} placeholder="Style Name" required type="text" name="style_name" id="style_name" className="form-control" value={styleName} />
                                <label htmlFor="style_name">Style Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleFabricChange} placeholder="Fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric} />
                                <label htmlFor="fabric">Fabric</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color} />
                                <label htmlFor="color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={handleImageChange} placeholder="Enter Hat Picture" required type="text" name="image" id="image" className="form-control" value={image} />
                                <label htmlFor="picture_url">Picture URL</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleLocationChange} required name="location" id="location" className="form-select" value={location}>
                                    <option value="">Choose a Location</option>
                                    {locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.href}>
                                                {location.closet_name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>
                            <button>Submit</button>


                        </form>
                    </div>
                </div>
            </div>
        </div>

    )
}
