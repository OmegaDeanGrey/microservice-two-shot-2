from django.db import models

# Create your models here.

class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True, default=None)
    closet_name = models.CharField(max_length=200, default=None)
    shelf_number = models.PositiveIntegerField(default=None)
    section_number = models.PositiveIntegerField( default=None)

    def __str__(self):
        return self.closet_name


class Hat(models.Model):
    fabric = models.CharField(max_length=100, default=None)
    style_name = models.CharField(max_length=100, default=None)
    color = models.CharField(max_length=25, default=None)
    image = models.URLField(default=None)
    location = models.ForeignKey(
        LocationVO,
        related_name="location",
        on_delete=models.CASCADE,
        default=None
    )

    def __str__(self):
        return self.style_name
